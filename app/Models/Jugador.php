<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    use HasFactory;

    protected $table = 'jugador';
    protected $fillable = [
        'apellidos',
        'nombres',
        'apodo',
        'fecha_nacimiento',
    ];

    public function JugadoresPorClub()
    {
        return $this->hasMany(JugadorPorClub::class, 'jugador_id', 'id')->orderBy('jugador_por_club.fecha_desde', 'desc');
    }

    public function clubes()
    {
        return $this->belongsToMany(Club::class, 'jugador_por_club', 'jugador_id', 'club_id')
            ->using(JugadorPorClub::class);
    }


}
