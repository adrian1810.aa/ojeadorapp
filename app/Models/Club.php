<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    use HasFactory;

    protected $table = 'club';
    protected $fillable = [
        'nombre',
        'ciudad',
        'pais',
        'apodo',
    ];



    public function JugadoresPorClub()
    {
        return $this->hasMany(JugadorPorClub::class, 'club_id', 'id');
    }
    public function JugadoresPorClubActivos()
    {
        return $this->hasMany(JugadorPorClub::class, 'club_id', 'id')->where('activo', true);
    }

    public function JugadoresPorClubInactivos()
    {
        return $this->hasMany(JugadorPorClub::class, 'club_id', 'id')->where('activo', false);
    }

    public function JugadoresPorClubAnterior()
    {
        return $this->hasMany(JugadorPorClub::class, 'club_anterior_id', 'id');
    }




}
