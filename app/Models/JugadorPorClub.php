<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JugadorPorClub extends Model
{
    use HasFactory;

    protected $table = "jugador_por_club";
    protected $fillable = [
        'jugador_id',
        'club_id',
        'club_anterior_id',
        'fecha_desde',
        'fecha_hasta',
        'activo',
    ];

    public function jugador()
    {
        return $this->belongsTo(Jugador::class, 'jugador_id', 'id');
    }

    public function club()
    {
        return $this->belongsTo(Club::class, 'club_id', 'id');
    }

    public function clubAnterior()
    {
        return $this->belongsTo(Club::class, 'club_anterior_id', 'id');
    }

}
