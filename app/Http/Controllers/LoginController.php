<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => true])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;

            return redirect('/')->with('message', $success);
        } else {
            return view('auth.login', ['validationError' => "Email o contraseña incorrecto, o usuario no activo"]);
        }
    }

    public function logout()
    {
        Auth::logout(); // Cierra la sesión del usuario
        return redirect()->route('login');
    }
}
