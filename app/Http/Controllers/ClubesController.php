<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Club;

class ClubesController extends Controller
{
    // Mostrar todos los registros
    public function index()
    {
        $clubes = Club::withCount('JugadoresPorClub')->get();
        return view('clubes.listar', compact('clubes'));
    }

    public function ClubesConFiltrosJSON(Request $request)
    {
        $opcionSeleccionada = $request->input('opcion');
        $filtro = $request->input('filtro');

        $clubes = Club::withCount('JugadoresPorClubActivos');
        if ($opcionSeleccionada == 'activo') {
            $clubes->has('JugadoresPorClubActivos', '>', 0);
        } elseif ($opcionSeleccionada == 'inactivo') {
            $clubes->has('JugadoresPorClubActivos', '=', 0);
        }

        if (strlen($filtro) > 0) {
            $clubes->where(function ($query) use ($filtro) {
                $query->where('id', 'ilike', '%' . $filtro . '%')
                    ->orwhere('nombre', 'ilike', '%' . $filtro . '%')
                    ->orwhere('pais', 'ilike', '%' . $filtro . '%')
                    ->orwhere('ciudad', 'ilike', '%' . $filtro . '%')
                    ->orwhere('apodo', 'ilike', '%' . $filtro . '%');
            });
        }


        $clubes = $clubes->get();

        // dd($clubes);
        return response()->json([
            'success' => true,
            'mensaje' => 'Listado de Clubes',
            'data' => $clubes
        ]);
    }

    public function ClubesConJugadores()
    {
        $clubes = Club::withCount('JugadoresPorClub')->has('JugadoresPorClub', '>', 0)->get();
        return view('clubes.listar', compact('clubes'));
    }

    public function ClubesSinJugadores()
    {

        $clubes = Club::withCount('JugadoresPorClub')->has('JugadoresPorClub', '=', 0)->get();
        return view('clubes.listar', compact('clubes'));
    }


    public function create()
    {
        return view('clubes.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'ciudad' => 'required',
            'pais' => 'required',
            'apodo' => 'required',
        ]);

        Club::create($request->all());

        return redirect()->route('clubes')
            ->with('success', 'Club creado correctamente');
    }

    // Mostrar un registro específico
    public function show($id)
    {
        $club = Club::findOrFail($id);
        return view('club.show', compact('club'));
    }

    public function edit($id)
    {
        $club = Club::findOrFail($id);
        return view('club.edit', compact('club'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required',
            'ciudad' => 'required',
            'pais' => 'required',
            'apodo' => 'required',
        ]);

        $club = Club::findOrFail($id);
        $club->update($request->all());

        return redirect()->route('club.index')
            ->with('success', 'Club actualizado correctamente');
    }

    public function destroy($id)
    {
        $club = Club::findOrFail($id);
        $club->delete();

        return redirect()->route('club.index')
            ->with('success', 'Club eliminado correctamente');
    }

}
