<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JugadorPorClub;
use App\Models\Jugador;
use App\Models\Club;


class JugadoresPorClubController extends Controller
{
    public function index()
    {
        $jugadoresPorClub = JugadorPorClub::all();
        return view('jugador_por_club.index', compact('jugadoresPorClub'));
    }

    public function create()
    {
        $jugadores = Jugador::all();
        $clubs = Club::all();
        return view('jugador_por_club.create', ["jugadores" => $jugadores, "clubs" => $clubs]);
    }

    public function store(Request $request)
    {
        if ($request->club_anterior_id = '') {
            $request->club_anterior_id = null;
        }

        $request->validate([
            'jugador_id' => 'required',
            'club_id' => 'required',
            'club_anterior_id' => 'nullable',
            'fecha_desde' => 'required|date',
            'fecha_hasta' => 'nullable|date',
            'activo' => 'required',
        ]);

        // dd($request->all());

        JugadorPorClub::create($request->all());

        return redirect()->route('jugadores')
            ->with('success', 'Relación Jugador-Club creada correctamente');
    }

    public function show($id)
    {
        $jugadorPorClub = JugadorPorClub::findOrFail($id);
        return view('jugador_por_club.show', compact('jugadorPorClub'));
    }

    public function edit($id)
    {
        $jugadorPorClub = JugadorPorClub::findOrFail($id);
        $jugadores = Jugador::all();
        $clubs = Club::all();
        return view('jugador_por_club.edit', compact('jugadorPorClub', 'jugadores', 'clubs'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'jugador_id' => 'required',
            'club_id' => 'required',
            'club_anterior_id' => 'nullable',
            'fecha_desde' => 'required|date',
            'fecha_hasta' => 'nullable|date',
            'activo' => 'required',
        ]);

        $jugadorPorClub = JugadorPorClub::findOrFail($id);
        $jugadorPorClub->update($request->all());

        return redirect()->route('jugador_por_club.index')
            ->with('success', 'Relación Jugador-Club actualizada correctamente');
    }

    public function destroy($id)
    {
        $jugadorPorClub = JugadorPorClub::findOrFail($id);
        $jugadorPorClub->delete();

        return redirect()->route('jugador_por_club.index')
            ->with('success', 'Relación Jugador-Club eliminada correctamente');
    }
}
