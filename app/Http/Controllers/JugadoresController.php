<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jugador;
use Illuminate\Support\Facades\DB;

class JugadoresController extends Controller
{

    public function index()
    {

        // $jugadores = Jugador::with('clubes');
        // $jugadores = Jugador::select(
        //     'jugador.*',
        //     DB::raw('count(jugador_por_club.*) as jugadores_por_club_count'),
        //     DB::raw('case when jugador_por_club.club_id = (select club_anterior_id 
        //     from jugador_por_club x 
        //     where x.jugador_id = jugador_por_club.jugador_id and x.club_id <> jugador_por_club.club_id) then
        //     true
        //     else false end as ocultar')
        // )
        //     ->leftJoin('jugador_por_club', 'jugador_por_club.jugador_id', '=', 'jugador.id')
        //     ->leftJoin('club', 'jugador_por_club.club_id', '=', 'club.id')
        //     ->groupBy('jugador.id', 'club.nombre', 'jugador_por_club.club_id', 'jugador_por_club.jugador_id')
        //     ->selectRaw("coalesce(club.nombre, 'Sin club') as nombre_club")
        //     ->orderby('jugador.id', 'asc')
        $jugadores = Jugador::select(
            'jugador.id',
            'jugador.apellidos',
            'jugador.nombres',
            'jugador.apodo',
            'jugador.fecha_nacimiento',
            DB::raw('MAX(jugador_por_club.fecha_desde) as fecha_desde'),
            DB::raw('COUNT(jugador_por_club.jugador_id) as jugadores_por_club_count'),
            DB::raw(
                "COALESCE(
                    (
                        SELECT club.nombre
                        FROM jugador_por_club x
                        LEFT JOIN club ON x.club_id = club.id
                        WHERE x.jugador_id = jugador.id
                        ORDER BY x.fecha_desde DESC
                        LIMIT 1
                    ),
                    'Sin club'
                ) AS nombre_club"
            )
        )
            ->leftJoin('jugador_por_club', 'jugador_por_club.jugador_id', '=', 'jugador.id')
            ->leftJoin('club', 'jugador_por_club.club_id', '=', 'club.id')
            ->groupBy('jugador.id', 'jugador.apellidos', 'jugador.nombres', 'jugador.apodo', 'jugador.fecha_nacimiento')
            ->orderBy('jugador.id', 'ASC')
            ->get();
        // dd($jugadores);
        return view('jugadores.listar', compact('jugadores'));
    }

    public function JugodoresConFiltrosJSON(Request $request)
    {
        $opcionSeleccionada = $request->query('opcion');
        $filtro = $request->query('filtro');

        // $jugadores = Jugador::select(
        //     'jugador.*',
        //     DB::raw('(select count(*)from jugador_por_club x where x.jugador_id = jugador.id) as jugadores_por_club_count'),
        //     DB::raw('case when jugador_por_club.club_id = (select club_anterior_id 
        //             from jugador_por_club x 
        //             where x.jugador_id = jugador_por_club.jugador_id 
        //             and x.club_id <> jugador_por_club.club_id) then true
        //             else false end as ocultar')
        // )
        //     ->leftJoin('jugador_por_club', 'jugador_por_club.jugador_id', '=', 'jugador.id')
        //     ->leftJoin('club', 'jugador_por_club.club_id', '=', 'club.id')
        //     ->groupBy('jugador.id', 'club.nombre', 'jugador_por_club.club_id', 'jugador_por_club.jugador_id')
        //     ->selectRaw("coalesce(club.nombre, 'Sin club') as nombre_club")
        //     ->orderby('jugador.id', 'asc');
        $jugadores = Jugador::select(
            'jugador.id',
            'jugador.apellidos',
            'jugador.nombres',
            'jugador.apodo',
            'jugador.fecha_nacimiento',
            DB::raw('MAX(jugador_por_club.fecha_desde) as fecha_desde'),
            DB::raw('COUNT(jugador_por_club.jugador_id) as jugadores_por_club_count'),
            DB::raw(
                "COALESCE(
                    (
                        SELECT club.nombre
                        FROM jugador_por_club x
                        LEFT JOIN club ON x.club_id = club.id
                        WHERE x.jugador_id = jugador.id
                        ORDER BY x.fecha_desde DESC
                        LIMIT 1
                    ),
                    'Sin club'
                ) AS nombre_club"
            )
        )
            ->leftJoin('jugador_por_club', 'jugador_por_club.jugador_id', '=', 'jugador.id')
            ->leftJoin('club', 'jugador_por_club.club_id', '=', 'club.id')
            ->groupBy('jugador.id', 'jugador.apellidos', 'jugador.nombres', 'jugador.apodo', 'jugador.fecha_nacimiento')
            ->orderBy('jugador.id', 'ASC');

        // $jugadores->where('jugador_por_club.activo', '=', true);
        if ($opcionSeleccionada == 'activo') {
            $jugadores->having(DB::raw('count(jugador_por_club.*)'), '>', 0);
        } elseif ($opcionSeleccionada == 'inactivo') {
            $jugadores->having(DB::raw('count(jugador_por_club.*)'), '=', 0);
        }

        if (strlen($filtro) > 0) {
            $jugadores->where(function ($query) use ($filtro) {
                $query->where('jugador.id', 'ilike', '%' . $filtro . '%')
                    ->orwhere('jugador.apellidos', 'ilike', '%' . $filtro . '%')
                    ->orwhere('jugador.nombres', 'ilike', '%' . $filtro . '%')
                    ->orwhere('jugador.apodo', 'ilike', '%' . $filtro . '%')
                    ->orwhere('jugador.fecha_nacimiento', 'ilike', '%' . $filtro . '%')
                    ->orwhere('club.nombre', 'ilike', '%' . $filtro . '%');
            });
        }

        $jugadores = $jugadores->get();

        // dd($jugadores);
        return response()->json([
            'success' => true,
            'mensaje' => 'Listado de Clubes',
            'data' => $jugadores
        ]);
    }


    public function create()
    {
        return view('jugadores.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'apellidos' => 'required',
            'nombres' => 'required',
            'apodo' => 'required',
            'fecha_nacimiento' => 'required|date',
        ]);

        Jugador::create($request->all());

        return redirect()->route('jugadores')
            ->with('success', 'Jugador creado correctamente');
    }

    public function show($id)
    {
        $jugador = Jugador::with('JugadoresPorClub.club')->findOrFail($id);
        return view('jugadores.show', ["jugador" => $jugador]);
    }

    public function edit($id)
    {
        $jugador = Jugador::findOrFail($id);
        return view('jugador.edit', ["jugador" => $jugador]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'apellidos' => 'required',
            'nombres' => 'required',
            'apodo' => 'required',
            'fecha_nacimiento' => 'required|date',
        ]);

        $jugador = Jugador::findOrFail($id);
        $jugador->update($request->all());

        return redirect()->route('jugador.index')
            ->with('success', 'Jugador actualizado correctamente');
    }

    public function destroy($id)
    {
        $jugador = Jugador::findOrFail($id);
        $jugador->delete();

        return redirect()->route('jugador.index')
            ->with('success', 'Jugador eliminado correctamente');
    }
}
