<?php

use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClubesController;
use App\Http\Controllers\JugadoresController;
use App\Http\Controllers\JugadoresPorClubController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('main');
})->name('inicio');

Route::get('/login', function () {
    return view('auth.login');
})->name('login');
route::post('login', [LoginController::class, 'login'])->name('login');
route::get('logout', [LoginController::class, 'logout'])->name('logout');

// Rutas para JugadorPorClub
Route::get('/jugadores_por_club', [JugadoresPorClubController::class, 'index']);
Route::get('/jugadores_por_club_create', [JugadoresPorClubController::class, 'create'])->name('jugadores_por_club_create');
Route::post('/jugadores_por_club', [JugadoresPorClubController::class, 'store'])->name('jugadores_por_club');
Route::put('/jugadores_por_club/{id}', [JugadoresPorClubController::class, 'update']);
Route::delete('/jugadores_por_club/{id}', [JugadoresPorClubController::class, 'destroy']);
Route::get('/jugadores_por_club/{id}', [JugadoresPorClubController::class, 'show']);

// Rutas para Jugador
Route::get('/jugadores', [JugadoresController::class, 'index'])->name('jugadores');
Route::get('/jugadores-filtrosjson', [JugadoresController::class, 'JugodoresConFiltrosJSON'])->name('JugodoresConFiltrosJSON');
Route::get('/jugadores-create', [JugadoresController::class, 'create'])->name('jugadores-create');
Route::post('/jugadores', [JugadoresController::class, 'store']);
Route::put('/jugadores/{id}', [JugadoresController::class, 'update']);
Route::delete('/jugadores/{id}', [JugadoresController::class, 'destroy']);
Route::get('/jugadores/{id}', [JugadoresController::class, 'show'])->name('Verjugador');

// Rutas para Club
Route::get('/clubes', [ClubesController::class, 'index'])->name('clubes');
Route::get('/clubes-filtrosjson', [ClubesController::class, 'ClubesConFiltrosJSON'])->name('ClubesConFiltrosJSON');
Route::get('/clubes-create', [ClubesController::class, 'create'])->name('clubes-create');
Route::post('/clubes', [ClubesController::class, 'store']);
Route::put('/clubes/{id}', [ClubesController::class, 'update']);
Route::delete('/clubes/{id}', [ClubesController::class, 'destroy']);
