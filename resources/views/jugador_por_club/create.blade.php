<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ficha</title>
    <!-- Tailwind -->
    <link rel="stylesheet" href="{{ asset('css/tailwind.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Font Awesome -->
    <script src="{{ asset('js/font-awesome.all.min.js') }}" crossorigin="anonymous"></script>
</head>

<body class="bg-gray-100 p-8">
    <div class="max-w-md mx-auto bg-white rounded p-8">
        <div class="mt-0">
            <a class="inline-block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                href="{{ route('jugadores') }}">
                Volver
            </a>
        </div>
        <h2 class="text-2xl font-bold mb-4">Formulario de Datos</h2>

        <form action="{{ route('jugadores_por_club') }}" method="POST">
            @csrf

            <div class="mb-4">
                <label for="jugador_id" class="block text-gray-700 text-sm font-bold mb-2">Jugador</label>
                <select name="jugador_id" id="jugador_id"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500">
                    <option value="">Seleccione una opcion</option>
                    @foreach ($jugadores as $jugador)
                        <option value="{{ $jugador->id }}">{{ $jugador->nombres . ' ' . $jugador->apellidos }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-4">
                <label for="club_id" class="block text-gray-700 text-sm font-bold mb-2">Club</label>
                <select name="club_id" id="club_id"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500">
                    <option value="">Seleccione una opcion</option>
                    @foreach ($clubs as $club)
                        <option value="{{ $club->id }}">{{ $club->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-4">
                <label for="club_anterior_id" class="block text-gray-700 text-sm font-bold mb-2">Club Anterior
                    (Opcional)</label>
                <select name="club_anterior_id" id="club_anterior_id"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500">
                    <option value="">Seleccione una opcion</option>
                    @foreach ($clubs as $club)
                        <option value="{{ $club->id }}">{{ $club->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-4">
                <label for="fecha_desde" class="block text-gray-700 text-sm font-bold mb-2">Fecha Desde</label>
                <input type="date" name="fecha_desde" id="fecha_desde"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500" required>
            </div>

            <div class="mb-4">
                <label for="fecha_hasta" class="block text-gray-700 text-sm font-bold mb-2">Fecha Hasta
                    (Opcional)</label>
                <input type="date" name="fecha_hasta" id="fecha_hasta"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500">
            </div>

            <div class="mb-4">
                <label for="activo" class="block text-gray-700 text-sm font-bold mb-2">Activo</label>
                <select name="activo" id="activo"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500">
                    <option value="1">Sí</option>
                    <option value="0">No</option>
                </select>
            </div>

            <div class="mt-6">
                <button type="submit" class="w-full bg-blue-500 text-white font-bold py-2 px-4 rounded">
                    Enviar Datos
                </button>
            </div>
        </form>
    </div>

</body>

</html>
