@auth
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Jugadores</title>

        <!-- Tailwind -->
        <link rel="stylesheet" href="{{ asset('css/tailwind.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- Font Awesome -->
        <script src="{{ asset('js/font-awesome.all.min.js') }}" crossorigin="anonymous"></script>
        <!-- alpine -->
        <script src="{{ asset('js/alpine.min.js') }}" defer></script>
        <style>
            .bg-sidebar {
                background: #3d68ff;
            }

            .cta-btn {
                color: #3d68ff;
            }

            .upgrade-btn {
                background: #1947ee;
            }

            .upgrade-btn:hover {
                background: #0038fd;
            }

            .active-nav-link {
                background: #1947ee;
            }

            .nav-item:hover {
                background: #1947ee;
            }

            .account-link:hover {
                background: #3d68ff;
            }
        </style>
        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>

    <body class="bg-gray-100 font-family-karla flex">

        <aside class="relative bg-sidebar h-screen w-64 hidden sm:block shadow-xl">
            <div class="p-6">
                <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Super
                    Ojeador</a>
                <button
                    class="w-full bg-white cta-btn font-semibold py-2 mt-5 rounded-br-lg rounded-bl-lg rounded-tr-lg shadow-lg hover:shadow-xl hover:bg-gray-300 flex items-center justify-center">
                    <i class="fas fa-plus mr-3"></i> New Report
                </button>
            </div>
            <nav class="text-white text-base font-semibold pt-3">
                <a href="{{ route('inicio') }}" class="flex items-center text-white py-4 pl-6 nav-item">
                    <i class="fas fa-futbol"></i>
                    Inicio
                </a>
                <a href="{{ route('clubes') }}"
                    class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                    <i class="fas fa-table mr-3"></i>
                    Clubes
                </a>
                <a href="{{ route('jugadores') }}"
                    class="flex items-center text-white active-nav-link opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                    <i class="fas fa-table mr-3"></i>
                    Jugadores
                </a>

            </nav>

        </aside>

        <div class="w-full flex flex-col h-screen overflow-y-hidden" x-data="start('{{ route('jugadores') }}', '{{ csrf_token() }}')">
            <!-- Desktop Header -->
            <header class="w-full items-center bg-white py-2 px-6 hidden sm:flex">
                <div class="w-1/2"></div>
                <div x-data="{ isOpen: false }" class="relative w-1/2 flex justify-end">
                    {{ auth()->user()->name }}
                    <button @click="isOpen = !isOpen"
                        class="realtive z-10 w-12 h-12 rounded-full overflow-hidden border-4 border-gray-400 hover:border-gray-300 focus:border-gray-300 focus:outline-none">
                        <img src="https://source.unsplash.com/uJ8LNVCBjFQ/400x400">
                    </button>
                    <button x-show="isOpen" @click="isOpen = false"
                        class="h-full w-full fixed inset-0 cursor-default"></button>
                    <div x-show="isOpen" class="absolute w-32 bg-white rounded-lg shadow-lg py-2 mt-16">
                        <a href="{{ route('logout') }}" class="block px-4 py-2 account-link hover:text-white">Sign Out</a>
                    </div>
                </div>
            </header>

            <!-- Mobile Header & Nav -->
            <header x-data="{ isOpen: false }" class="w-full bg-sidebar py-5 px-6 sm:hidden">
                <div class="flex items-center justify-between">
                    <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Admin</a>
                    <button @click="isOpen = !isOpen" class="text-white text-3xl focus:outline-none">
                        <i x-show="!isOpen" class="fas fa-bars"></i>
                        <i x-show="isOpen" class="fas fa-times"></i>
                    </button>
                </div>

                <!-- Dropdown Nav -->
                <nav :class="isOpen ? 'flex' : 'hidden'" class="flex flex-col pt-4">
                    <a href="{{ route('inicio') }}" class="flex items-center text-white py-4 pl-6 nav-item">
                        <i class="fas fa-tachometer-alt mr-3"></i>
                        Inicio
                    </a>
                    <a href="{{ route('clubes') }}"
                        class="flex items-center text-white opacity-75 active-nav-link hover:opacity-100 py-4 pl-6 nav-item">
                        <i class="fas fa-sticky-note mr-3"></i>
                        Clubes
                    </a>
                    <a href="{{ route('jugadores') }}"
                        class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                        <i class="fas fa-table mr-3"></i>
                        Jugadores
                    </a>

                </nav>

            </header>

            <div class="w-full overflow-x-hidden border-t flex flex-col">
                <main class="w-full flex-grow p-6">
                    <div class="w-full mt-12" style="margin-top: 0;">
                        <p class="text-xl pb-3 flex items-center">
                            <i class="fas fa-list mr-3"></i> Jugadores
                        </p>
                        <button
                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded
                        {{ auth()->user()->role !== 'modifier' ? 'disabled-button' : '' }}"
                            type="submit" onclick="window.location = '{{ route('jugadores-create') }}';">
                            Crear Nuevo
                        </button>

                        <button
                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded
                        {{ auth()->user()->role !== 'modifier' ? 'disabled-button' : '' }}"
                            type="submit" onclick="window.location = '{{ route('jugadores_por_club_create') }}';">
                            Nueva Ficha
                        </button>
                        <div class="my-2 flex sm:flex-row flex-col">
                            <div class="block relative px-4 py-0">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="opcion">Selecciona una
                                    opción</label>
                                <select name="opcion" id="opcion"
                                    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                                    <option value="todos" selected>Todos</option>
                                    <option value="activo">Con jugadores Activos</option>
                                    <option value="inactivo">Sin jugadores Activos</option>
                                </select>

                            </div>

                            <div class="block relative px-4 py-7">

                                <input id="filtro" name="filtro" placeholder="Search"
                                    class="appearance-none rounded-r rounded-l sm:rounded-l-none border border-gray-400 border-b block pl-8 pr-6 py-2 w-full bg-white text-sm placeholder-gray-400 text-gray-700 focus:bg-white focus:placeholder-gray-600 focus:text-gray-700 focus:outline-none" />
                            </div>

                            <div class="block relative px-4 py-7">
                                <!-- Botón -->
                                <div class="mb-4">
                                    <button type="button" @click="buscar('{{ route('JugodoresConFiltrosJSON') }}')"
                                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                                        Aplicar Filtros
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bg-white overflow-auto">
                        <table id="tabla" class="min-w-full bg-white shadow-md rounded-lg overflow-hidden">
                            <thead class="bg-gray-800 text-white">
                                <tr>
                                    <th class="text-left py-3 px-1 uppercase font-semibold text-sm">ID</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Apellidos</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Nombres</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Apodo</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Fecha Nacimiento</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Club Activo</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Cant. Clubes
                                    </th>
                                    {{-- <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Ocultar
                                </th> --}}
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-700">
                                <template x-for="jugador in jugadores" :key="jugador.id">
                                    <tr x-show="!jugador.ocultar">
                                        <td class="text-left py-3 px-1" x-text="jugador.id"></td>
                                        <td class="text-left py-3 px-1" x-text="jugador.apellidos"></td>
                                        <td class="text-left py-3 px-1" x-text="jugador.nombres"></td>
                                        <td class="text-left py-3 px-1" x-text="jugador.apodo"></td>
                                        <td class="text-left py-3 px-1" x-text="jugador.fecha_nacimiento"></td>
                                        <td class="text-left py-3 px-1" x-text="jugador.nombre_club">
                                        <td class="text-left py-3 px-1" x-text="jugador.jugadores_por_club_count">
                                            {{-- <td class="text-left py-3 px-1" x-text="jugador.ocultar"> --}}
                                        </td>
                                        <td class="text-left py-3 px-1">
                                            <a class="bg-green-500 text-white py-1 px-2 rounded"
                                                x-bind:href="'/jugadores/' + jugador.id">Ver</a>
                                        </td>
                                    </tr>
                                </template>
                            </tbody>
                        </table>

                    </div>
            </div>
            </main>

            <footer class="w-full bg-white text-right p-4">
                Built by <a target="_blank" href="https://davidgrzyb.com" class="underline">David Grzyb</a>.
            </footer>
        </div>

        </div>


    </body>
    <!-- Vista Blade -->

    <script>
        const csrfToken = document.head.querySelector('meta[name="csrf-token"]').content;

        function start(url) {
            console.log("URL:", url, "token, ", csrfToken)
            return {
                jugadores: [],
                init(url) {
                    this.buscar(url);
                },
                buscar(url) {
                    var selectBox = document.getElementById("opcion");
                    var opcionSeleccionada = selectBox.options[selectBox.selectedIndex];
                    const filtroValor = document.getElementById('filtro').value;
                    console.log(opcionSeleccionada.value, filtroValor);
                    const queryParams =
                        `opcion=${encodeURIComponent(opcionSeleccionada.value)}&filtro=${encodeURIComponent(filtroValor)}`;
                    url = `${url}?${queryParams}`;
                    console.log(url);
                    fetch(url, {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': csrfToken,
                            }
                        })
                        .then(response => response.json())
                        .then(data => {
                            console.log(data);
                            if (data.success) {
                                this.jugadores = data.data;
                                console.log(this.jugadores);
                            } else {
                                console.error(data.mensaje);
                            }
                        })
                        .catch(error => {
                            console.error('Error en la solicitud:', error);
                        });

                }
            };
        }
    </script>


    </html>
@else
    <script>
        window.location = "{{ route('login') }}";
    </script>
@endauth
