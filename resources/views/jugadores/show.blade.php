@auth
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Jugadores</title>

        <!-- Tailwind -->
        <link rel="stylesheet" href="{{ asset('css/tailwind.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- Font Awesome -->
        <script src="{{ asset('js/font-awesome.all.min.js') }}" crossorigin="anonymous"></script>
        <!-- alpine -->
        <script src="{{ asset('js/alpine.min.js') }}" defer></script>
        <style>
            .bg-sidebar {
                background: #3d68ff;
            }

            .cta-btn {
                color: #3d68ff;
            }

            .upgrade-btn {
                background: #1947ee;
            }

            .upgrade-btn:hover {
                background: #0038fd;
            }

            .active-nav-link {
                background: #1947ee;
            }

            .nav-item:hover {
                background: #1947ee;
            }

            .account-link:hover {
                background: #3d68ff;
            }
        </style>
        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>

    <body class="bg-gray-100 font-family-karla flex">

        <aside class="relative bg-sidebar h-screen w-64 hidden sm:block shadow-xl">
            <div class="p-6">
                <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Super
                    Ojeador</a>
                <button
                    class="w-full bg-white cta-btn font-semibold py-2 mt-5 rounded-br-lg rounded-bl-lg rounded-tr-lg shadow-lg hover:shadow-xl hover:bg-gray-300 flex items-center justify-center">
                    <i class="fas fa-plus mr-3"></i> New Report
                </button>
            </div>
            <nav class="text-white text-base font-semibold pt-3">
                <a href="{{ route('inicio') }}" class="flex items-center text-white py-4 pl-6 nav-item">
                    <i class="fas fa-tachometer-alt mr-3"></i>
                    Inicio
                </a>

                <a href="{{ route('clubes') }}"
                    class="flex items-center text-white opacity-75  hover:opacity-100 py-4 pl-6 nav-item">
                    <i class="fas fa-sticky-note mr-3"></i>
                    Clubes
                </a>

                <a href="{{ route('jugadores') }}"
                    class="flex items-center text-white opacity-75 active-nav-link hover:opacity-100 py-4 pl-6 nav-item">
                    <i class="fas fa-sticky-note mr-3"></i>
                    Jugadores
                </a>

            </nav>

        </aside>

        <div class="w-full flex flex-col h-screen overflow-y-hidden">
            <!-- Desktop Header -->
            <header class="w-full items-center bg-white py-2 px-6 hidden sm:flex">
                <div class="w-1/2"></div>
                <div x-data="{ isOpen: false }" class="relative w-1/2 flex justify-end">
                    {{ auth()->user()->name }}
                    <button @click="isOpen = !isOpen"
                        class="realtive z-10 w-12 h-12 rounded-full overflow-hidden border-4 border-gray-400 hover:border-gray-300 focus:border-gray-300 focus:outline-none">
                        <img src="https://source.unsplash.com/uJ8LNVCBjFQ/400x400">
                    </button>
                    <button x-show="isOpen" @click="isOpen = false"
                        class="h-full w-full fixed inset-0 cursor-default"></button>
                    <div x-show="isOpen" class="absolute w-32 bg-white rounded-lg shadow-lg py-2 mt-16">
                        <a href="{{ route('logout') }}" class="block px-4 py-2 account-link hover:text-white">Sign Out</a>
                    </div>
                </div>
            </header>

            <!-- Mobile Header & Nav -->
            <header x-data="{ isOpen: false }" class="w-full bg-sidebar py-5 px-6 sm:hidden">
                <div class="flex items-center justify-between">
                    <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Admin</a>
                    <button @click="isOpen = !isOpen" class="text-white text-3xl focus:outline-none">
                        <i x-show="!isOpen" class="fas fa-bars"></i>
                        <i x-show="isOpen" class="fas fa-times"></i>
                    </button>
                </div>

                <!-- Dropdown Nav -->
                <nav :class="isOpen ? 'flex' : 'hidden'" class="flex flex-col pt-4">
                    <a href="{{ route('inicio') }}" class="flex items-center text-white py-4 pl-6 nav-item">
                        <i class="fas fa-tachometer-alt mr-3"></i>
                        Inicio
                    </a>
                    <a href="{{ route('clubes') }}"
                        class="flex items-center text-white opacity-75 active-nav-link hover:opacity-100 py-4 pl-6 nav-item">
                        <i class="fas fa-sticky-note mr-3"></i>
                        Clubes
                    </a>
                    <a href="{{ route('jugadores') }}"
                        class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                        <i class="fas fa-table mr-3"></i>
                        Jugadores
                    </a>

                </nav>

            </header>

            <div class="w-full overflow-x-hidden border-t flex flex-col">
                <div class="mt-0">
                    <a class="inline-block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        href="{{ route('jugadores') }}">
                        Volver
                    </a>
                </div>
                <main class="w-full flex-grow px-6">
                    <div class="w-full mt-0">
                        <h1 class="text-3xl font-bold mb-4">Información del Jugador</h1>

                        <div class="mb-4">
                            <p class="text-lg"><i class="fas fa-user mr-2"></i>Apellidos: {{ $jugador->apellidos }}</p>
                            <p class="text-lg"><i class="fas fa-user mr-2"></i>Nombres: {{ $jugador->nombres }}</p>
                            <p class="text-lg"><i class="fas fa-user-ninja mr-2"></i>Apodo: {{ $jugador->apodo }}</p>
                            <p class="text-lg"><i class="fas fa-calendar-alt mr-2"></i>Fecha de Nacimiento:
                                {{ $jugador->fecha_nacimiento }}</p>
                        </div>
                        <h2 class="text-2xl font-bold mb-2">Registros de Jugador por Club</h2>
                    </div>
                    <div class="bg-white overflow-auto">
                        <table id="tabla" class="min-w-full bg-white shadow-md rounded-lg overflow-hidden">
                            <thead class="bg-gray-800 text-white">
                                <tr>
                                    <th class="text-left py-3 px-1 uppercase font-semibold text-sm">Club</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">País</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Fecha Desde</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Fecha Hasta</th>
                                    <th class="text-left py-3 px-4 uppercase font-semibold text-sm">Activo</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-700">
                                @foreach ($jugador->JugadoresPorClub as $registro)
                                    <tr>
                                        <td class="py-2 px-4 border-b">{{ $registro->club->nombre }}</td>
                                        <td class="py-2 px-4 border-b">{{ $registro->club->pais }}</td>
                                        <td class="py-2 px-4 border-b">{{ $registro->fecha_desde }}</td>
                                        <td class="py-2 px-4 border-b">{{ $registro->fecha_hasta }}</td>
                                        <td
                                            class="py-2 px-4 border-b 
                                        {{ $registro->activo ? 'bg-green-200 hover:bg-green-300' : 'bg-red-200 hover:bg-red-300' }}">
                                            {{ $registro->activo ? 'Sí' : 'No' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
            </div>
            </main>

            <footer class="w-full bg-white text-right p-4">
                Built by <a target="_blank" href="https://davidgrzyb.com" class="underline">David Grzyb</a>.
            </footer>
        </div>

        </div>


    </body>

    </html>
@else
    <script>
        window.location = "{{ route('login') }}";
    </script>
@endauth
