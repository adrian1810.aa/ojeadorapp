<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jugador</title>
    <!-- Tailwind -->
    <link rel="stylesheet" href="{{ asset('css/tailwind.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Font Awesome -->
    <script src="{{ asset('js/font-awesome.all.min.js') }}" crossorigin="anonymous"></script>
</head>

<body class="bg-gray-100 p-8">

    <div class="max-w-md mx-auto bg-white rounded p-8">
        <div class="mt-0">
            <a class="inline-block bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                href="{{ route('jugadores') }}">
                Volver
            </a>
        </div>
        <h2 class="text-2xl font-bold mb-4">Formulario de Datos</h2>

        <form action="{{ route('jugadores') }}" method="POST">
            @csrf

            <div class="mb-4">
                <label for="apellidos" class="block text-gray-700 text-sm font-bold mb-2">Apellidos</label>
                <input type="text" name="apellidos" id="apellidos"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500" required>
            </div>

            <div class="mb-4">
                <label for="nombres" class="block text-gray-700 text-sm font-bold mb-2">Nombres</label>
                <input type="text" name="nombres" id="nombres"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500" required>
            </div>

            <div class="mb-4">
                <label for="apodo" class="block text-gray-700 text-sm font-bold mb-2">Apodo</label>
                <input type="text" name="apodo" id="apodo"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500" required>
            </div>

            <div class="mb-4">
                <label for="fecha_nacimiento" class="block text-gray-700 text-sm font-bold mb-2">Fecha de
                    Nacimiento</label>
                <input type="date" name="fecha_nacimiento" id="fecha_nacimiento"
                    class="w-full border p-2 rounded focus:outline-none focus:border-blue-500" required>
            </div>

            <div class="mt-6">
                <button type="submit" class="w-full bg-blue-500 text-white font-bold py-2 px-4 rounded">
                    Enviar Datos
                </button>
            </div>
        </form>
    </div>

</body>

</html>
