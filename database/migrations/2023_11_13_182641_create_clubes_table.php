<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('club', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 50)->comment('Nombre del club');
            $table->string('ciudad', 60)->comment('Ciudad donde se ubica el club');
            $table->string('pais', 60)->comment('Pais al cual pertenece el club');
            $table->string('apodo', 40)->nullable()->comment('apodo del club');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clubes');
    }
};
