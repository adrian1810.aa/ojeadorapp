<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jugador', function (Blueprint $table) {
            $table->id();
            $table->string('apellidos', 100)->comment('apellido del jugador');
            $table->string('nombres', 100)->comment('nombres del jugador');
            $table->string('apodo', 40)->nullable()->comment('apodo del jugador');
            $table->date('fecha_nacimiento')->nullable()->comment('fecha de nacimiento del jugador');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jugadores');
    }
};
