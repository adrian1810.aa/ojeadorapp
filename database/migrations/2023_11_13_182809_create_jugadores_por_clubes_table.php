<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jugador_por_club', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('jugador_id')->comment('clave foranea de la tabla jugadores');
            $table->unsignedBigInteger('club_id')->comment('clave foranea de la tabla clubes');
            $table->unsignedBigInteger('club_anterior_id')->nullable()->comment('clave foranea del club anterior');
            $table->foreign('jugador_id')->references('id')->on('jugador');
            $table->foreign('club_id')->references('id')->on('club');
            $table->foreign('club_anterior_id')->references('id')->on('club');
            $table->date('fecha_desde')->comment('Fecha de inicio contrato del jugador con el club');
            $table->date('fecha_hasta')->nullable()->comment('Fecha de culminacion del contrato del jugador con el club');
            $table->boolean('activo')->comment('Indica si esta actualmente en el club o no')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jugadores_por_clubes');
    }
};
