<?php

namespace Database\Seeders;

use App\Models\JugadorPorClub;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JugadoresPorClubesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $jugadoresPorClub = [
            [
                'jugador_id' => 1,
                'club_id' => 34,
                'club_anterior_id' => null,
                'fecha_desde' => '31/01/2019',
                'fecha_hasta' => null,
                'activo' => true,
            ],
            [
                'jugador_id' => 2,
                'club_id' => 2,
                'club_anterior_id' => null,
                'fecha_desde' => '22/01/2022',
                'fecha_hasta' => null,
                'activo' => true,
            ],

            [
                'jugador_id' => 3,
                'club_id' => 31,
                'club_anterior_id' => 1,
                'fecha_desde' => '08/07/2022',
                'fecha_hasta' => null,
                'activo' => true,
            ],

            [
                'jugador_id' => 3,
                'club_id' => 1,
                'club_anterior_id' => null,
                'fecha_desde' => '27/10/2018',
                'fecha_hasta' => '08/07/2022',
                'activo' => true,
            ],

            [
                'jugador_id' => 4,
                'club_id' => 4,
                'club_anterior_id' => null,
                'fecha_desde' => '11/01/2019',
                'fecha_hasta' => null,
                'activo' => true,
            ],
            [
                'jugador_id' => 5,
                'club_id' => 38,
                'club_anterior_id' => 35,
                'fecha_desde' => '20/07/2022',
                'fecha_hasta' => null,
                'activo' => true,
            ],
        ];

        foreach ($jugadoresPorClub as $jugadorData) {
            JugadorPorClub::create($jugadorData);
        }

    }
}
