<?php

namespace Database\Seeders;

use App\Models\Club;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClubesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $clubes = [
            ['nombre' => 'River Plate', 'ciudad' => 'Buenos Aires', 'apodo' => 'Millonarios', 'pais' => 'Argentina'],
            //1
            ['nombre' => 'Boca Juniors', 'ciudad' => 'Buenos Aires', 'apodo' => 'Xeneizes', 'pais' => 'Argentina'],
            //2
            ['nombre' => 'Independiente', 'ciudad' => 'Avellaneda', 'apodo' => 'Rojo', 'pais' => 'Argentina'],
            //3

            ['nombre' => 'Flamengo', 'ciudad' => 'Río de Janeiro', 'apodo' => 'Rubronegro', 'pais' => 'Brasil'],
            //4
            ['nombre' => 'Corinthians', 'ciudad' => 'São Paulo', 'apodo' => 'Timão', 'pais' => 'Brasil'],
            ['nombre' => 'São Paulo', 'ciudad' => 'São Paulo', 'apodo' => 'Tricolor', 'pais' => 'Brasil'],

            ['nombre' => 'Colo Colo', 'ciudad' => 'Santiago', 'apodo' => 'Popular', 'pais' => 'Chile'],
            ['nombre' => 'Universidad de Chile', 'ciudad' => 'Santiago', 'apodo' => 'La U', 'pais' => 'Chile'],
            ['nombre' => 'Universidad Católica', 'ciudad' => 'Santiago', 'apodo' => 'La Franja', 'pais' => 'Chile'],

            ['nombre' => 'Atlético Nacional', 'ciudad' => 'Medellín', 'apodo' => 'Verdolaga', 'pais' => 'Colombia'],
            ['nombre' => 'Millonarios', 'ciudad' => 'Bogotá', 'apodo' => 'Embajador', 'pais' => 'Colombia'],
            ['nombre' => 'América de Cali', 'ciudad' => 'Cali', 'apodo' => 'Estudiantil', 'pais' => 'Colombia'],

            ['nombre' => 'Barcelona', 'ciudad' => 'Guayaquil', 'apodo' => 'Torero', 'pais' => 'Ecuador'],
            ['nombre' => 'Liga de Quito', 'ciudad' => 'Quito', 'apodo' => 'Albo', 'pais' => 'Ecuador'],
            ['nombre' => 'Emelec', 'ciudad' => 'Guayaquil', 'apodo' => 'Bombillo', 'pais' => 'Ecuador'],

            ['nombre' => 'Club América', 'ciudad' => 'Ciudad de México', 'apodo' => 'Azulcrema', 'pais' => 'Mexico'],
            ['nombre' => 'Chivas', 'ciudad' => 'Guadalajara', 'apodo' => 'Rojiblanco', 'pais' => 'Mexico'],
            ['nombre' => 'Monterrey', 'ciudad' => 'Monterrey', 'apodo' => 'Rayados', 'pais' => 'Mexico'],

            ['nombre' => 'Olimpia', 'ciudad' => 'Asunción', 'apodo' => 'Decano', 'pais' => 'Paraguay'],
            ['nombre' => 'Cerro Porteño', 'ciudad' => 'Asunción', 'apodo' => 'Ciclón', 'pais' => 'Paraguay'],
            ['nombre' => 'Libertad', 'ciudad' => 'Asunción', 'apodo' => 'Gumarelo', 'pais' => 'Paraguay'],

            ['nombre' => 'Alianza Lima', 'ciudad' => 'Lima', 'apodo' => 'Íntimos', 'pais' => 'Peru'],
            ['nombre' => 'Sporting Cristal', 'ciudad' => 'Lima', 'apodo' => 'Celestes', 'pais' => 'Peru'],
            ['nombre' => 'Universitario de Deportes', 'ciudad' => 'Lima', 'apodo' => 'cremas', 'pais' => 'Peru'],

            ['nombre' => 'Bayern Munich', 'ciudad' => 'Múnich', 'apodo' => 'Bávaros', 'pais' => 'Alemania'],
            ['nombre' => 'Borussia Dortmund', 'ciudad' => 'Dortmund', 'apodo' => 'Borussia', 'pais' => 'Alemania'],
            ['nombre' => 'RB Leipzig', 'ciudad' => 'Leipzig', 'apodo' => 'Die Roten Bullen', 'pais' => 'Alemania'],

            ['nombre' => 'Real Madrid', 'ciudad' => 'Madrid', 'apodo' => 'Merengues', 'pais' => 'España'],
            ['nombre' => 'Barcelona', 'ciudad' => 'Barcelona', 'apodo' => 'Culés', 'pais' => 'España'],
            ['nombre' => 'Atlético de Madrid', 'ciudad' => 'Madrid', 'apodo' => 'Colchoneros', 'pais' => 'España'],

            ['nombre' => 'Manchester City', 'ciudad' => 'Manchester', 'apodo' => 'Cityzens', 'pais' => 'Inglaterra'],
            ['nombre' => 'Liverpool', 'ciudad' => 'Liverpool', 'apodo' => 'Reds', 'pais' => 'Inglaterra'],
            ['nombre' => 'Manchester United', 'ciudad' => 'Manchester', 'apodo' => 'Red Devils', 'pais' => 'Inglaterra'],
            ['nombre' => 'Newcastle United Football Club', 'ciudad' => 'Newcastle upon Tyne', 'apodo' => 'urracas', 'pais' => 'Inglaterra'],
            //34

            ['nombre' => 'Juventus', 'ciudad' => 'Turín', 'apodo' => 'Bianconeri', 'pais' => 'Italia'],
            ['nombre' => 'Inter de Milán', 'ciudad' => 'Milán', 'apodo' => 'Nerazzurri', 'pais' => 'Italia'],
            ['nombre' => 'AC Milan', 'ciudad' => 'Milán', 'apodo' => 'Rossoneri', 'pais' => 'Italia'],
            ['nombre' => 'Associazione Sportiva Roma', 'ciudad' => 'Roma', 'apodo' => 'I Giallorossi', 'pais' => 'Italia'],

            ['nombre' => 'Paris Saint-Germain', 'ciudad' => 'París', 'apodo' => 'Les Parisiens', 'pais' => 'Francia'],
            ['nombre' => 'Olympique de Lyon', 'ciudad' => 'Lyon', 'apodo' => 'Les Gones', 'pais' => 'Francia'],
            ['nombre' => 'Club Internacional de Fútbol Miami', 'ciudad' => 'Miami', 'apodo' => 'Las Garzas', 'pais' => 'EEUU'],

            ['nombre' => 'Al-Nassr Football Club', 'ciudad' => 'Riad', 'apodo' => 'Al-Alami', 'pais' => 'Arabia'],
        ];

        foreach ($clubes as $clubData) {
            Club::create($clubData);
        }

    }
}
