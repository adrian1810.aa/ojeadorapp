<?php

namespace Database\Seeders;

use App\Models\Jugador;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JugadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $jugadores = [
            ['apellidos' => 'Almirón', 'nombres' => 'Miguel', 'apodo' => 'Miggy', 'fecha_nacimiento' => '1994-07-10'],
            ['apellidos' => 'Benedetto', 'nombres' => 'Darío', 'apodo' => 'El Pipa', 'fecha_nacimiento' => '1990-03-16'],
            ['apellidos' => 'Álvarez', 'nombres' => 'Julián', 'apodo' => 'La Araña', 'fecha_nacimiento' => '2000-01-31'],
            ['apellidos' => 'Barbosa', 'nombres' => 'Gabriel', 'apodo' => 'Gabigol', 'fecha_nacimiento' => '1996-08-03'],
            ['apellidos' => 'Dybala', 'nombres' => 'Paulo', 'apodo' => 'La Joya', 'fecha_nacimiento' => '1993-11-15'],
            ['apellidos' => 'Lewandowski', 'nombres' => 'Robert', 'apodo' => 'Bobek', 'fecha_nacimiento' => '1988-08-21'],
            ['apellidos' => 'Messi', 'nombres' => 'Lionel Andrés', 'apodo' => 'La Pulga', 'fecha_nacimiento' => '1987-06-24'],
            ['apellidos' => 'Dos Santos Aveiro', 'nombres' => 'Cristiano Ronaldo', 'apodo' => 'El Bicho', 'fecha_nacimiento' => '1985-02-05'],
            ['apellidos' => 'William Bellingham', 'nombres' => 'Jude Victor', 'apodo' => 'El Bicho', 'fecha_nacimiento' => '1985-02-05'],
            ['apellidos' => 'Páez Gavira', 'nombres' => 'Pablo Martín', 'apodo' => 'Gavi', 'fecha_nacimiento' => '2004-08-05'],
        ];

        foreach ($jugadores as $jugadorData) {
            Jugador::create($jugadorData);
        }
    }
}
