<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Hash;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@ucgre.is4.py',
                'password' => Hash::make('123456'),
                'active' => true,
                'role' => 'modifier',
            ],
            [
                'name' => 'Invitado',
                'email' => 'invitado@ucgre.is4.py',
                'password' => Hash::make('123456'),
                'active' => true,
                'role' => 'viewer',
            ],
            [
                'name' => 'Usuario Inactivo',
                'email' => 'inactivo@ucgre.is4.py',
                'password' => Hash::make('123456'),
                'active' => false,
                'role' => 'viewer',
            ],
        ];

        foreach ($users as $userData) {
            User::create($userData);
        }

    }
}
